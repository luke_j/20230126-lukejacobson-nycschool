//
//  SchoolViewController.swift
//  NYCSchool
//
//  Created by Luke Jacobson on 1/26/23.
//

import Foundation
import UIKit
import MapKit

class SchoolViewController: UIViewController {
    
    @IBOutlet weak var nav: UINavigationItem!
    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var Address1: UILabel!
    @IBOutlet weak var address2: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var websiteLabel: UILabel!
    @IBOutlet weak var attendanceLabel: UILabel!
    @IBOutlet weak var graduationLabel: UILabel!
    @IBOutlet weak var collegeLabel: UILabel!
    @IBOutlet weak var satReadingLabel: UILabel!
    @IBOutlet weak var satMathLabel: UILabel!
    @IBOutlet weak var satWritingLabel: UILabel!
    
    var schoolInfo:[String: Any]!;
    
    override func viewDidLoad() {
        super.viewDidLoad();
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateScores), name: NSNotification.Name.SATDataLoaded, object: nil);
    }
    
    //I would have made an object to store and parse the school info with more time, sorry about the mess :(
    override func viewWillAppear(_ animated: Bool) {
        //sets up the map
        if let latitudeString = schoolInfo["Latitude"] as? String {
            if let latitude = Double(latitudeString) {//I had issues casting directly to a Double so I did this dumb workaround
                if let longitudeString = schoolInfo["Longitude"] as? String {
                    if let longitude = Double(longitudeString) {
                        let location = CLLocationCoordinate2D(latitude: CLLocationDegrees(latitude), longitude: CLLocationDegrees(longitude));
                        let region = MKCoordinateRegion(center: location, latitudinalMeters: 250, longitudinalMeters: 250);
                        map.setRegion(region, animated: false);
                    }
                }
            }
        }
        
        //sets the title
        if let name = schoolInfo["school_name"] as? String {
            nav.title = name;
        }
        
        //these next few if statements fill out the address
        if let address = schoolInfo["primary_address_line_1"] as? String {
            Address1.text = address;
        }
        
        if let city = schoolInfo["city"] as? String {
            if let state = schoolInfo["state_code"] as? String {
                if let postCode = schoolInfo["zip"] as? String {
                    address2.text = city + ", " + state + " " + postCode;
                }
            }
        }
        
        if let phone = schoolInfo["phone_number"] as? String {
            phoneLabel.text = phone;
        }
        
        if let email = schoolInfo["school_email"] as? String {
            emailLabel.text = email;
        }
        
        if let website = schoolInfo["website"] as? String {
            websiteLabel.text = website;
        }
        
        //Same issue here casting directly to double :(
        if let attendence = schoolInfo["attendance_rate"] as? String {
            if let attendenceNum = Double(attendence) {
                let percent =  Int(attendenceNum * 100)
                attendanceLabel.text = "Attendence Rate: " + String(percent) + "%";
            }
        }
        
        if let gradRate = schoolInfo["graduation_rate"] as? String {
            if let gradRateNum = Double(gradRate) {
                let percent =  Int(gradRateNum * 100)
                graduationLabel.text = "Graduation Rate: " + String(percent) + "%";
            }
        }
        
        if let collegeRate = schoolInfo["college_career_rate"] as? String {
            if let collegeRateNum = Double(collegeRate) {
                let percent =  Int(collegeRateNum * 100)
                collegeLabel.text = "College/Career Rate: " + String(percent) + "%";
            }
        }
        
        if let dbn = schoolInfo["dbn"] as? String {
            if let scores = SchoolList.satScores[dbn] {
                if var readingScore = scores["sat_critical_reading_avg_score"] as? String {
                    if readingScore == "s" {
                        readingScore = "N/A"
                    }
                    satReadingLabel.text = "Reading: " + readingScore;
                }
                if var mathScore = scores["sat_math_avg_score"] as? String {
                    if mathScore == "s" {
                        mathScore = "N/A"
                    }
                    satMathLabel.text = "Math: " + mathScore;
                }
                if var writingScore = scores["sat_writing_avg_score"] as? String{
                    if writingScore == "s" {
                        writingScore = "N/A"
                    }
                    satWritingLabel.text = "Writing: " + writingScore;
                }
            }
        }
    }
    
    //selector for when the SAT scores are loaded
    @objc
    private func updateScores() {
        if let dbn = schoolInfo["dbn"] as? String {
            if let scores = SchoolList.satScores[dbn] {
                if let readingScore = scores["sat_critical_reading_avg_score"] as? String {
                    satReadingLabel.text = "Reading: " + readingScore;
                }
                if let mathScore = scores["sat_math_avg_score"] as? String {
                    satMathLabel.text = "Math: " + mathScore;
                }
                if let writingScore = scores["sat_writing_avg_score"] as? String{
                    satWritingLabel.text = "Writing: " + writingScore;
                }
            }
        }
    }
}
