//
//  SchoolList.swift
//  NYCSchool
//
//  Created by Luke Jacobson on 1/26/23.
//

import Foundation
import UIKit
import SODAKit

struct SchoolList {
    static var query = SODAQuery(client: client, dataset: "s3k6-pzi2");
    static var client = SODAClient(domain: "data.cityofnewyork.us", token: "hZ2eizfG9t6sT1SPi5gN8E21j");
    static var satQuery = SODAQuery(client: client, dataset: "f9bf-2cp4")
    static var schools = [[String:Any]]();
    static var satScores = [String: [String: Any]]();
}
