//
//  ViewController.swift
//  NYCSchool
//
//  Created by Luke Jacobson on 1/26/23.
//

import UIKit
import MapKit

class ViewController: UIViewController {
    
    @IBOutlet weak var table: UITableView!;
    
    //Sets up the view and adds an observer to wait for the table's data to fill
    override func viewDidLoad() {
        super.viewDidLoad();
        // Do any additional setup after loading the view.
        table.dataSource = self;
        table.reloadData();
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateTable), name: NSNotification.Name.SchoolDataLoaded, object: nil);
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let cell = sender as? SchoolTableCell {
            if let dest = segue.destination as? SchoolViewController {
                dest.schoolInfo = cell.schoolInfo;
                
                if let dbn = cell.schoolInfo["dbn"] as? String {
                    if let _ = SchoolList.satScores[dbn] {
                    } else {
                        SchoolList.satQuery
                            .select("dbn, sat_critical_reading_avg_score, sat_math_avg_score, sat_writing_avg_score")
                            .filterColumn("dbn", dbn).get { res in
                                switch res {
                                case .dataset (let data):
                                    if data.count > 0 {
                                        SchoolList.satScores[dbn] = data[0];
                                        NotificationCenter.default.post(name: NSNotification.Name.SATDataLoaded, object: nil);
                                    }
                                case .error (let error):
                                    print(error);
                                }
                                
                            };
                    }
                }
            }
        }
    }
    
    //selector to update the table once the data comes in
    @objc
    private func updateTable() {
        table.reloadData();
        fillTable();
    }
    
    //With more time I would have done proper pagination with Prefetching
    private func fillTable() {
        SchoolList.query.orderAscending("school_name")
            .select("school_name, primary_address_line_1, Latitude, Longitude, city, zip, state_code, school_email, website, attendance_rate, graduation_rate, college_career_rate, phone_number, dbn")
            .get { res in
                switch res {
                case .dataset (let data):
                    SchoolList.schools = data;
                    self.table.reloadData();
                case .error (let error):
                    print(error);
                }
            };
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        SchoolList.schools.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "School", for: indexPath);
        
        if let schoolCell = cell as? SchoolTableCell {
            if let schoolName = SchoolList.schools[indexPath.row]["school_name"] as? String {
                schoolCell.label.text = schoolName;
                schoolCell.schoolInfo = SchoolList.schools[indexPath.row];
                schoolCell.parent = self;
                return schoolCell;
            }
        }
        
        return cell;
    }
    
    
}

class SchoolTableCell: UITableViewCell {
    
    @IBOutlet weak var label: UILabel!
    
    @IBAction func selectSchool(_ sender: Any) {
        parent.performSegue(withIdentifier: "openSchool", sender: self);
    }
    
    var schoolInfo:[String: Any]!;
    var parent:ViewController!;
}
